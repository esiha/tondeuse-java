package fr.esiha.tondeuse.domain;

import java.util.List;

import static java.util.List.copyOf;

public final class Mower {
    private Position position;
    private final List<Instruction> instructions;

    public Mower(Position startPosition, List<Instruction> instructions) {
        this.position = startPosition;
        this.instructions = copyOf(instructions);
    }

    void runOn(RectangularLawn lawn) {
        assertMowerIsWithinLimits(lawn);
        instructions.forEach(instruction -> moveIfPossible(instruction.execute(position), lawn));
    }

    private void assertMowerIsWithinLimits(RectangularLawn lawn) {
        if (!lawn.isWithinLimits(position.getPoint())) {
            throw new IllegalArgumentException("Mower is outside of Lawn.");
        }
    }

    private void moveIfPossible(final Position newPosition, final RectangularLawn lawn) {
        if (lawn.isWithinLimits(newPosition.getPoint())) {
            position = newPosition;
        }
    }

    public Position getPosition() {
        return position;
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }
}
