package fr.esiha.tondeuse.domain;

import java.util.function.Function;

public enum Instruction {
    MOVE(Position::translateOnce),
    TURN_LEFT(Position::quarterTurnLeft),
    TURN_RIGHT(Position::quarterTurnRight);

    private final Function<Position, Position> operation;

    Instruction(final Function<Position, Position> operation) {
        this.operation = operation;
    }

    Position execute(Position position) {
        return operation.apply(position);
    }
}
