package fr.esiha.tondeuse.domain;

import java.util.Objects;

public final class Position {
    private final Point point;
    private final Orientation orientation;

    private Position(Point point, Orientation orientation) {
        this.point = point;
        this.orientation = orientation;
    }

    public static Position of(Point point, Orientation orientation) {
        return new Position(point, orientation);
    }

    public static Position of(int abscissa, int ordinate, Orientation orientation) {
        return of(Point.of(abscissa, ordinate), orientation);
    }

    public Position translateOnce() {
        return of(point.plus(orientation.getTranslationUnit()), orientation);
    }

    public Position quarterTurnLeft() {
        return of(point, orientation.quarterTurnLeft());
    }

    public Position quarterTurnRight() {
        return of(point, orientation.quarterTurnRight());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Position position = (Position) o;
        return Objects.equals(point, position.point) && orientation == position.orientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(point, orientation);
    }

    @Override
    public String toString() {
        return String.format("Position(%s, %s)", point, orientation);
    }

    public Point getPoint() {
        return point;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}
