package fr.esiha.tondeuse.domain;

import java.util.List;

import static java.util.stream.Collectors.toUnmodifiableList;

public final class Gardener {
    private final RectangularLawn lawn;
    private final List<Mower> mowers;

    public Gardener(RectangularLawn lawn, List<Mower> mowers) {
        this.lawn = lawn;
        this.mowers = mowers;
    }

    public void runMowers() {
        mowers.forEach(mower -> mower.runOn(lawn));
    }

    public List<Position> mowersPositions() {
        return mowers.stream().map(Mower::getPosition).collect(toUnmodifiableList());
    }

    public RectangularLawn getLawn() {
        return lawn;
    }

    public List<Mower> getMowers() {
        return mowers;
    }
}
