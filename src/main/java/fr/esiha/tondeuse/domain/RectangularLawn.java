package fr.esiha.tondeuse.domain;

import java.util.Objects;

public final class RectangularLawn {
    private static final Point BOTTOM_LEFT_CORNER = Point.of(0, 0);
    private final Point topRightCorner;

    public RectangularLawn(final Point topRightCorner) {
        this.topRightCorner = requireInTopRightQuadrantOfOrigin(topRightCorner);
    }

    private static Point requireInTopRightQuadrantOfOrigin(Point topRightCorner) {
        if (!topRightCorner.isInTopRightQuadrantOf(BOTTOM_LEFT_CORNER)) {
            throw new IllegalArgumentException("Top right corner must be in the top right quadrant of origin.");
        }
        return topRightCorner;
    }

    public static RectangularLawn of(Point topRightCorner) {
        return new RectangularLawn(topRightCorner);
    }

    public boolean isWithinLimits(Point point) {
        return point.isInTopRightQuadrantOf(BOTTOM_LEFT_CORNER)
                && point.isInBottomLeftQuadrantOf(topRightCorner);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final RectangularLawn that = (RectangularLawn) o;
        return Objects.equals(topRightCorner, that.topRightCorner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topRightCorner);
    }

    @Override
    public String toString() {
        return String.format("RectangularLawn(%s)", topRightCorner);
    }
}
