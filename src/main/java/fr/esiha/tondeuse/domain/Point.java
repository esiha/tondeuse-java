package fr.esiha.tondeuse.domain;

import java.util.Objects;

public final class Point {
    private final int abscissa;
    private final int ordinate;

    private Point(int abscissa, int ordinate) {
        this.abscissa = abscissa;
        this.ordinate = ordinate;
    }

    public static Point of(int abscissa, int ordinate) {
        return new Point(abscissa, ordinate);
    }

    public Point plus(Point other) {
        return Point.of(this.abscissa + other.abscissa, this.ordinate + other.ordinate);
    }

    public boolean isInTopRightQuadrantOf(Point other) {
        return other.abscissa <= this.abscissa && other.ordinate <= this.ordinate;
    }

    public boolean isInBottomLeftQuadrantOf(Point other) {
        return other.abscissa >= this.abscissa && other.ordinate >= this.ordinate;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Point point = (Point) o;
        return abscissa == point.abscissa && ordinate == point.ordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(abscissa, ordinate);
    }

    @Override
    public String toString() {
        return String.format("Point(%d, %d)", abscissa, ordinate);
    }

    public int getAbscissa() {
        return abscissa;
    }

    public int getOrdinate() {
        return ordinate;
    }
}
