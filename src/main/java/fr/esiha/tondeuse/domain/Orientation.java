package fr.esiha.tondeuse.domain;

public enum Orientation {
    NORTH(Point.of(0, 1)) {
        @Override
        public Orientation quarterTurnLeft() {
            return WEST;
        }

        @Override
        public Orientation quarterTurnRight() {
            return EAST;
        }
    },
    EAST(Point.of(1, 0)) {
        @Override
        public Orientation quarterTurnLeft() {
            return NORTH;
        }

        @Override
        public Orientation quarterTurnRight() {
            return SOUTH;
        }
    },
    SOUTH(Point.of(0, -1)) {
        @Override
        public Orientation quarterTurnLeft() {
            return EAST;
        }

        @Override
        public Orientation quarterTurnRight() {
            return WEST;
        }
    },
    WEST(Point.of(-1, 0)) {
        @Override
        public Orientation quarterTurnLeft() {
            return SOUTH;
        }

        @Override
        public Orientation quarterTurnRight() {
            return NORTH;
        }
    };

    private final Point translationUnit;

    Orientation(final Point translationUnit) {
        this.translationUnit = translationUnit;
    }

    public abstract Orientation quarterTurnLeft();

    public abstract Orientation quarterTurnRight();

    Point getTranslationUnit() {
        return translationUnit;
    }
}
