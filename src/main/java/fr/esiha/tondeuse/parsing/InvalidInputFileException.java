package fr.esiha.tondeuse.parsing;

public final class InvalidInputFileException extends RuntimeException {
    public InvalidInputFileException(final String message) {
        super(message);
    }

    public InvalidInputFileException(final Throwable cause) {
        super(cause);
    }
}
