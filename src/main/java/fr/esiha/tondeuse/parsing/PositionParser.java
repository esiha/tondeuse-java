package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Orientation;
import fr.esiha.tondeuse.domain.Position;

import static fr.esiha.tondeuse.domain.Orientation.*;

final class PositionParser {
    public Position parsePosition(String input) {
        final var parts = input.split(" ");
        return Position.of(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), parseOrientation(parts[2]));
    }

    private Orientation parseOrientation(String input) {
        switch (input) {
            case "N":
                return NORTH;
            case "E":
                return EAST;
            case "S":
                return SOUTH;
            case "W":
                return WEST;
            default:
                throw new InvalidInputFileException("Invalid orientation: " + input);
        }
    }
}
