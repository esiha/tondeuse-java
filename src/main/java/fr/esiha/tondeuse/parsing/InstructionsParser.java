package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Instruction;

import java.util.Arrays;
import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static java.util.stream.Collectors.toUnmodifiableList;

final class InstructionsParser {
    public List<Instruction> parseInstructions(String input) {
        return Arrays.stream(input.split("")).map(this::parseInstruction).collect(toUnmodifiableList());
    }

    private Instruction parseInstruction(String input) {
        switch (input) {
            case "A":
                return MOVE;
            case "D":
                return TURN_RIGHT;
            case "G":
                return TURN_LEFT;
            default:
                throw new InvalidInputFileException("Invalid instruction: " + input);
        }
    }
}
