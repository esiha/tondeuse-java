package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Point;

final class PointParser {
    public Point parsePoint(String input) {
        final var numbers = requireSize2(input.split("\\s+"));

        return Point.of(parseNumber(numbers[0]), parseNumber(numbers[1]));
    }

    private static String[] requireSize2(String[] input) {
        if (input.length != 2) {
            throw new InvalidInputFileException("A Point needs exactly 2 numbers.");
        }
        return input;
    }

    private int parseNumber(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            throw new InvalidInputFileException(e);
        }
    }
}
