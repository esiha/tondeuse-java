package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Gardener;
import fr.esiha.tondeuse.domain.Mower;
import fr.esiha.tondeuse.domain.RectangularLawn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class GardenerFileParser {
    public Gardener parseGardener(Path file) {
        final var lines = getLines(file);
        assertCorrectNumberOfLines(lines);
        return parseGardener(lines);
    }

    private List<String> getLines(final Path file) {
        final List<String> lines;
        try {
            lines = Files.readAllLines(file);
        } catch (IOException e) {
            throw new InvalidInputFileException(e);
        }
        return lines;
    }

    private void assertCorrectNumberOfLines(final List<String> lines) {
        if (lines.isEmpty()) {
            throw new InvalidInputFileException("Lawn description is missing.");
        } else if (lines.size() % 2 == 0) {
            throw new InvalidInputFileException("Expected an odd number of lines");
        }
    }

    private Gardener parseGardener(final List<String> lines) {
        return new Gardener(parseLawn(lines.get(0)), parseMowers(lines));
    }

    private RectangularLawn parseLawn(final String input) {
        return RectangularLawn.of(new PointParser().parsePoint(input));
    }

    private List<Mower> parseMowers(final List<String> input) {
        final var mowers = new ArrayList<Mower>();
        for (int i = 1; i < input.size(); i += 2) {
            mowers.add(new Mower(
                    new PositionParser().parsePosition(input.get(i)),
                    new InstructionsParser().parseInstructions(input.get(i + 1))
            ));
        }
        return mowers;
    }
}
