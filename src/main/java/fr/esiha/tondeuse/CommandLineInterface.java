package fr.esiha.tondeuse;

import fr.esiha.tondeuse.parsing.GardenerFileParser;

import java.nio.file.Path;

public final class CommandLineInterface {
    public static void main(String[] args) {
        final var gardener = new GardenerFileParser()
                .parseGardener(Path.of(args[0]));

        gardener.runMowers();
        gardener.mowersPositions().forEach(position ->
                System.out.printf("%d %d %s%n",
                        position.getPoint().getAbscissa(),
                        position.getPoint().getOrdinate(),
                        position.getOrientation().name().charAt(0)
                )
        );
    }
}
