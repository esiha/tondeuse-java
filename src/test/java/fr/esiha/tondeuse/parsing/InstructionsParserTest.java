package fr.esiha.tondeuse.parsing;

import org.junit.jupiter.api.Test;

import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class InstructionsParserTest {
    private final InstructionsParser parser = new InstructionsParser();

    @Test
    void shouldFailToParseInvalidInstruction() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parseInstructions("AZERTY")
        ).withMessageContaining("Invalid instruction: Z");
    }

    @Test
    void shouldParseValidInstructions() {
        assertThat(parser.parseInstructions("ADGDDAG"))
                .isEqualTo(List.of(MOVE, TURN_RIGHT, TURN_LEFT, TURN_RIGHT, TURN_RIGHT, MOVE, TURN_LEFT));
    }
}
