package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Orientation;
import fr.esiha.tondeuse.domain.Position;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class PositionParserTest {
    private final PositionParser parser = new PositionParser();

    @Test
    void shouldFailToParseInvalidOrientation() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parsePosition("3 5 Z")
        ).withMessageContaining("Invalid orientation: Z");
    }

    @ParameterizedTest
    @CsvSource({"3 5 N,3,5,NORTH", "1 4 E,1,4,EAST", "2 2 S,2,2,SOUTH", "4 4 W,4,4,WEST"})
    void shouldParseValidPosition(String input, int abscissa, int ordinate, Orientation orientation) {
        assertThat(parser.parsePosition(input)).isEqualTo(Position.of(abscissa, ordinate, orientation));
    }
}
