package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Point;
import fr.esiha.tondeuse.domain.Position;
import fr.esiha.tondeuse.domain.RectangularLawn;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static fr.esiha.tondeuse.domain.Orientation.EAST;
import static fr.esiha.tondeuse.domain.Orientation.NORTH;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

public class GardenerFileParserTest {
    private final GardenerFileParser parser = new GardenerFileParser();

    @Test
    void shouldFailToParseEmptyFile() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parseGardener(file(""))
        ).withMessageContaining("Lawn description is missing");
    }

    @Test
    void shouldFailToParseWhenTheNumberOfLinesIsEven() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parseGardener(file(
                        "5 5\n" +
                                "2 2 N"
                ))
        ).withMessageContaining("Expected an odd number of lines");
    }

    @Test
    void shouldParseValidFile() throws IOException {
        final var gardener = parser.parseGardener(file(
                "5 5\n" +
                        "1 2 N\n" +
                        "GAGAGAGAA\n" +
                        "3 3 E\n" +
                        "AADAADADDA"
        ));

        assertSoftly(softly -> {
            softly.assertThat(gardener.getLawn()).isEqualTo(RectangularLawn.of(Point.of(5, 5)));

            final var mower1 = gardener.getMowers().get(0);
            softly.assertThat(mower1.getPosition()).isEqualTo(Position.of(1, 2, NORTH));
            softly.assertThat(mower1.getInstructions()).isEqualTo(List.of(TURN_LEFT, MOVE, TURN_LEFT, MOVE, TURN_LEFT, MOVE, TURN_LEFT, MOVE, MOVE));

            final var mower2 = gardener.getMowers().get(1);
            softly.assertThat(mower2.getPosition()).isEqualTo(Position.of(3, 3, EAST));
            softly.assertThat(mower2.getInstructions()).isEqualTo(List.of(MOVE, MOVE, TURN_RIGHT, MOVE, MOVE, TURN_RIGHT, MOVE, TURN_RIGHT, TURN_RIGHT, MOVE));
        });
    }

    private static Path file(String contents) throws IOException {
        return Files.writeString(Files.createTempFile(null, null), contents);
    }
}
