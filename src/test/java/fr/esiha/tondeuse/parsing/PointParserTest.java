package fr.esiha.tondeuse.parsing;

import fr.esiha.tondeuse.domain.Point;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class PointParserTest {

    private final PointParser parser = new PointParser();

    @Test
    void shouldFailToParseInvalidPoint() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parsePoint("A 0")
        ).withCauseInstanceOf(NumberFormatException.class);
    }

    @Test
    void shouldFailToParseTooFewNumbers() {
        assertThatExceptionOfType(InvalidInputFileException.class).isThrownBy(() ->
                parser.parsePoint("1")
        ).withMessageContaining("exactly 2 numbers");
    }

    @ParameterizedTest
    @CsvSource({"1 2,1,2", "3 5,3,5"})
    void shouldParseValidPoint(String input, int abscissa, int ordinate) {
        assertThat(parser.parsePoint(input)).isEqualTo(Point.of(abscissa, ordinate));
    }
}
