package fr.esiha.tondeuse.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

public class RectangularLawnTest {
    @Test
    void shouldCompareEqualToAnotherWithSameCoordinates() {
        Assertions.assertThat(RectangularLawn.of(Point.of(3, 5)))
                .isNotNull()
                .isEqualTo(RectangularLawn.of(Point.of(3, 5)));
    }

    @Test
    void shouldHaveReadableStringRepresentation() {
        final var topRightCorner = Point.of(2, 9);
        assertThat(RectangularLawn.of(topRightCorner))
                .hasToString(String.format("RectangularLawn(%s)", topRightCorner));
    }

    @ParameterizedTest
    @MethodSource("pointWithinLimitsArguments")
    void shouldTellWhenAPointIsWithinLimits(Point point) {
        assertThat(RectangularLawn.of(Point.of(2, 4)).isWithinLimits(point)).isTrue();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> pointWithinLimitsArguments() {
        return List.of(
                Point.of(0, 0),
                Point.of(0, 1),
                Point.of(1, 0),
                Point.of(2, 4),
                Point.of(1, 4),
                Point.of(2, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("pointNotWithinLimitsArguments")
    void shouldTellWhenAPointIsNotWithinLimits(Point point) {
        assertThat(RectangularLawn.of(Point.of(2, 4)).isWithinLimits(point)).isFalse();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> pointNotWithinLimitsArguments() {
        return List.of(
                Point.of(0, -1),
                Point.of(-1, 0),
                Point.of(-1, -1),
                Point.of(3, 4),
                Point.of(2, 5),
                Point.of(3, 5)
        );
    }

    @Test
    void shouldFailToCreateWithTopRightCornerNotInTopRightQuadrantOfOrigin() {
        assertThatIllegalArgumentException().isThrownBy(() ->
                RectangularLawn.of(Point.of(-1, 0))
        ).withMessageContaining("Top right corner must be in the top right quadrant of origin.");
    }
}
