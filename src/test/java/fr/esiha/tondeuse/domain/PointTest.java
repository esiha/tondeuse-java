package fr.esiha.tondeuse.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PointTest {
    @Test
    void shouldCompareEqualToAnotherWithSameValues() {
        Assertions.assertThat(Point.of(1, 2)).isNotNull().isEqualTo(Point.of(1, 2));
    }

    @Test
    void shouldHaveReadableStringRepresentation() {
        assertThat(Point.of(3, 4)).hasToString("Point(3, 4)");
    }

    @Test
    void shouldAddToAnotherPoint() {
        assertThat(Point.of(3, 5).plus(Point.of(-1, 2)))
                .isEqualTo(Point.of(2, 7));
    }

    @ParameterizedTest
    @MethodSource("anotherPointIsInTopRightQuadrantArguments")
    void shouldTellWhenAnotherPointIsInTopRightQuadrant(Point other) {
        assertThat(Point.of(3, 2).isInTopRightQuadrantOf(other)).isTrue();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> anotherPointIsInTopRightQuadrantArguments() {
        return List.of(
                Point.of(3, 2),
                Point.of(3, 1),
                Point.of(2, 2),
                Point.of(2, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("anotherPointIsNotInTopRightQuadrantArguments")
    void shouldTellWhenAnotherPointIsNotInTopRightQuadrant(Point other) {
        assertThat(Point.of(3, 2).isInTopRightQuadrantOf(other)).isFalse();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> anotherPointIsNotInTopRightQuadrantArguments() {
        return List.of(
                Point.of(4, 2),
                Point.of(3, 3),
                Point.of(4, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("anotherPointIsInBottomLeftQuadrantArguments")
    void shouldTellWhenAnotherPointIsInBottomLeftQuadrant(Point other) {
        assertThat(Point.of(3, 2).isInBottomLeftQuadrantOf(other)).isTrue();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> anotherPointIsInBottomLeftQuadrantArguments() {
        return List.of(
                Point.of(3, 2),
                Point.of(4, 2),
                Point.of(3, 3),
                Point.of(4, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("anotherPointIsNotInBottomLeftQuadrantArguments")
    void shouldTellWhenAnotherPointIsNotInBottomLeftQuadrant(Point other) {
        assertThat(Point.of(3, 2).isInBottomLeftQuadrantOf(other)).isFalse();
    }

    @SuppressWarnings("unused")
    private static Iterable<Point> anotherPointIsNotInBottomLeftQuadrantArguments() {
        return List.of(
                Point.of(2, 2),
                Point.of(3, 1),
                Point.of(2, 1)
        );
    }
}
