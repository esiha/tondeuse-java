package fr.esiha.tondeuse.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static fr.esiha.tondeuse.domain.Orientation.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class OrientationTest {
    @ParameterizedTest
    @MethodSource("quarterTurnLeftArguments")
    void shouldQuarterTurnLeft(Orientation start, Orientation expected) {
        assertThat(start.quarterTurnLeft()).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> quarterTurnLeftArguments() {
        return List.of(
                arguments(NORTH, WEST),
                arguments(EAST, NORTH),
                arguments(SOUTH, EAST),
                arguments(WEST, SOUTH)
        );
    }

    @ParameterizedTest
    @MethodSource("quarterTurnRightArguments")
    void shouldQuarterTurnRight(Orientation start, Orientation expected) {
        assertThat(start.quarterTurnRight()).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> quarterTurnRightArguments() {
        return List.of(
                arguments(NORTH, EAST),
                arguments(EAST, SOUTH),
                arguments(SOUTH, WEST),
                arguments(WEST, NORTH)
        );
    }


    @ParameterizedTest
    @MethodSource("translationUnitArguments")
    void shouldOfferTranslationUnit(Orientation orientation, Point translationUnit) {
        assertThat(orientation.getTranslationUnit()).isEqualTo(translationUnit);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> translationUnitArguments() {
        return List.of(
                arguments(NORTH, Point.of(0, 1)),
                arguments(EAST, Point.of(1, 0)),
                arguments(SOUTH, Point.of(0, -1)),
                arguments(WEST, Point.of(-1, 0))
        );
    }
}
