package fr.esiha.tondeuse.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static fr.esiha.tondeuse.domain.Orientation.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class PositionTest {
    @Test
    void shouldCompareEqualToAnotherWithSameProperties() {
        Assertions.assertThat(Position.of(Point.of(1, 2), NORTH))
                .isNotNull()
                .isEqualTo(Position.of(Point.of(1, 2), NORTH));
    }

    @Test
    void shouldHaveReadableStringRepresentation() {
        final var point = Point.of(3, 5);
        assertThat(Position.of(point, SOUTH))
                .hasToString(String.format("Position(%s, SOUTH)", point));
    }

    @Test
    void shouldOfferFactoryThatEncapsulatesPointCreation() {
        assertThat(Position.of(2, 4, NORTH))
                .isEqualTo(Position.of(Point.of(2, 4), NORTH));
    }

    @ParameterizedTest
    @MethodSource("translateOnceArguments")
    void shouldTranslateOnce(Position start, Position expected) {
        assertThat(start.translateOnce()).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> translateOnceArguments() {
        final var point = Point.of(3, 4);
        return List.of(
                arguments(Position.of(point, EAST), Position.of(point.plus(EAST.getTranslationUnit()), EAST)),
                arguments(Position.of(point, SOUTH), Position.of(point.plus(SOUTH.getTranslationUnit()), SOUTH))
        );
    }

    @ParameterizedTest
    @MethodSource("quarterTurnLeftArguments")
    void shouldQuarterTurnLeft(Position start, Position expected) {
        assertThat(start.quarterTurnLeft()).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> quarterTurnLeftArguments() {
        final var point = Point.of(2, 6);
        return List.of(
                arguments(Position.of(point, NORTH), Position.of(point, NORTH.quarterTurnLeft())),
                arguments(Position.of(point, WEST), Position.of(point, WEST.quarterTurnLeft()))
        );
    }

    @ParameterizedTest
    @MethodSource("quarterTurnRightArguments")
    void shouldQuarterTurnRight(Position start, Position expected) {
        assertThat(start.quarterTurnRight()).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> quarterTurnRightArguments() {
        final var point = Point.of(5, 4);
        return List.of(
                arguments(Position.of(point, NORTH), Position.of(point, NORTH.quarterTurnRight())),
                arguments(Position.of(point, SOUTH), Position.of(point, SOUTH.quarterTurnRight()))
        );
    }
}
