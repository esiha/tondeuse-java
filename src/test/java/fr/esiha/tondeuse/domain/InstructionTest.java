package fr.esiha.tondeuse.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static fr.esiha.tondeuse.domain.Orientation.EAST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class InstructionTest {
    @ParameterizedTest
    @MethodSource("executeArguments")
    void shouldExecute(Instruction instruction, Position expected) {
        assertThat(instruction.execute(Position.of(2, 5, EAST))).isEqualTo(expected);
    }

    @SuppressWarnings("unused")
    private static Iterable<Arguments> executeArguments() {
        return List.of(
                arguments(MOVE, Position.of(2, 5, EAST).translateOnce()),
                arguments(TURN_LEFT, Position.of(2, 5, EAST).quarterTurnLeft()),
                arguments(TURN_RIGHT, Position.of(2, 5, EAST).quarterTurnRight())
        );
    }
}
