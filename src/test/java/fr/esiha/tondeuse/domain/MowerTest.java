package fr.esiha.tondeuse.domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static fr.esiha.tondeuse.domain.Orientation.*;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

public class MowerTest {
    @Test
    void shouldRunOnLawn() {
        final var startPosition = Position.of(8, 1, WEST);
        final var instructions = List.of(MOVE, MOVE, TURN_LEFT, MOVE);
        final var mower = new Mower(startPosition, instructions);

        mower.runOn(RectangularLawn.of(Point.of(9, 9)));

        assertThat(mower.getPosition())
                .isEqualTo(MOVE.execute(TURN_LEFT.execute(MOVE.execute(MOVE.execute(startPosition)))));
    }

    @Test
    void shouldIgnoreInstructionsWhenNecessaryToRemainInsideTheLawn() {
        final var startPosition = Position.of(3, 3, NORTH);
        final var mower = new Mower(startPosition, List.of(MOVE, MOVE, TURN_RIGHT));

        mower.runOn(RectangularLawn.of(Point.of(3, 3)));

        assertThat(mower.getPosition()).isEqualTo(TURN_RIGHT.execute(startPosition));
    }

    @Test
    void shouldFailToRunWhenStartingOutsideOfLawn() {
        assertThatIllegalArgumentException().isThrownBy(() ->
                new Mower(Position.of(5, 5, EAST), emptyList()).runOn(RectangularLawn.of(Point.of(4, 4)))
        ).withMessageContaining("Mower is outside of Lawn");
    }
}
