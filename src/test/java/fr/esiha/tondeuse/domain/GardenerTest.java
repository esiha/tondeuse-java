package fr.esiha.tondeuse.domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static fr.esiha.tondeuse.domain.Instruction.*;
import static fr.esiha.tondeuse.domain.Orientation.NORTH;
import static fr.esiha.tondeuse.domain.Orientation.EAST;
import static org.assertj.core.api.Assertions.assertThat;

public class GardenerTest {
    @Test
    void shouldRunMowersOnTheLawn() {
        final var gardener = new Gardener(
                RectangularLawn.of(Point.of(5, 5)),
                List.of(
                        new Mower(
                                Position.of(1, 2, NORTH),
                                List.of(TURN_LEFT, MOVE, TURN_LEFT, MOVE, TURN_LEFT, MOVE, TURN_LEFT, MOVE, MOVE)
                        ),
                        new Mower(
                                Position.of(3, 3, EAST),
                                List.of(MOVE, MOVE, TURN_RIGHT, MOVE, MOVE, TURN_RIGHT, MOVE, TURN_RIGHT, TURN_RIGHT, MOVE)
                        )
                )
        );

        gardener.runMowers();

        assertThat(gardener.mowersPositions())
                .isEqualTo(List.of(Position.of(1, 3, NORTH), Position.of(5, 1, EAST)));
    }
}
